(function()
{
	jQuery(document).ready(function()
	{
		var h = [],
			w = jQuery('body').text().trim().replace(/<(.*?)>/g, ' ').replace(/\&nbsp\;/g, ' ').replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g, ' ').replace(/\s+/g, ' ');
		w = w.split(' ').filter(function(e)
		{
			return (e != null) && (e.length >= 1);
		});
		h[h.length] = 'Trang có <b>' + w.length + '</b> từ.';
		h[h.length] = '<b>' + jQuery('h1').length + '</b> thẻ heading 1.';
		h[h.length] = '<b>' + jQuery('h2').length + '</b> thẻ heading 2.';
		h[h.length] = '<b>' + jQuery('h3').length + '</b> thẻ heading 3.';
		h[h.length] = '<b>' + jQuery('p').length + '</b> thẻ paragraph.';
		jQuery('body').append('<div style="display:block;position:fixed;z-index:9998;right:0;top:0;padding:20px 35px 20px 20px;background-color:rgba(0,0,0,0.8);color:rgba(255,255,255,0.9);font-family:Tahoma,Verdana,Arial,sans-serif;font-size:16px;line-height:1.5;border:1px solid rgba(255,255,255,0.35);border-top:none;border-right:none;box-shadow:0 0 3px #000" id="b33eb58ffac1435cb5fe50bef795af1f"><ul style="margin:0;padding:0"><li style="list-style-type:disc;list-style-position:inside">' + h.join('</li><li style="list-style-type:disc;list-style-position:inside">') + '</li></ul><a href="javascript:{void(0);}" onclick="document.getElementById(\'b33eb58ffac1435cb5fe50bef795af1f\').remove();" title="Đóng" style="display:block;position:absolute;top:0;right:0;width:24px;height:24px;line-height:24px;text-align:center;background-color:rgba(255,255,255,0.3);font-weight:normal;color:#FF0">x</a></div>');
	});
})();
